<?php require_once (__DIR__ . '/vendor/autoload.php');

$bot = new \tbf\Main("607547381:AAFSeIHPXRzIY_FtA4a4polfXTi4P8jRKh8", "@VixedFitness_bot");
//$telegram = new Telegram('607547381:AAFSeIHPXRzIY_FtA4a4polfXTi4P8jRKh8');


$bot->cmd('/start', function() use ($bot) {
    $message_welcome = "Привет! Я бот фитнес-клуба TEST_BOT <YOUR_NAME>! Я могу рассказать тебе о наших тренировках, записать тебя на понравившиеся занятия и напомнить о предстоящих посещениях. С чего начнем? ";
    $keyboard = [
        [
            ['text' => '🏋️Клубы'],
            ['text' => '👫Занятия'],
        ],
        [
            ['text' => '💆Массажисты'],
            ['text' => '🏅Трениры'],
        ],
        [
            ['text' => '👑Скидки'],
            ['text' => '⚙Личный кабинет'],
        ],

    ];
    $options = [
        'reply_markup' => ["keyboard"=>$keyboard, 'resize_keyboard'=>true],
    ];
    $bot->sendMessage($message_welcome, $options);
});

$bot->on('callback', function($callback) use ($bot) { $bot->callback($callback); });

$bot->cmd('myloc', function($text) use ($bot) {
    $bot->sendLocation(-7.61, 109.51);
});

$bot->cmd("*", function($text) use ($bot) {
    if( $text == "🏋️Клубы" ) {
        $keyboard[] = [
            ['text' => 'Золотая роза', 'callback_data' => 'club:gold_rouse'],
            ['text' => 'Большой дом', 'callback_data' => 'club:big_house'],
        ];
        $options = [
            'reply_markup' => ['inline_keyboard' => $keyboard],
        ];
        $bot->sendMessage("Вот наши клубы, которые есть на данный момент: ", $options);
    }
    if( $text == "/ph" ) {
        $file = new CURLFile(realpath("photos/yppolus.jpg"));
        $bot->sendPhoto($file);
    }
    if( $text == "⚙Личный кабинет" ) {
        $keyboard[] = [
            ['text' => 'TEST', 'callback_data' => 'test'],
        ];
        $options = [
            'reply_markup' => ['inline_keyboard' => $keyboard],
        ];
        $bot->sendMessage('TEST API', $options);
    }
});

$bot->run();