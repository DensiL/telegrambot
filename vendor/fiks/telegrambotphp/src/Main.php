<?php
namespace tbf;

use Bot;
use MysqliDb;

class Main {

    const DB_HOST     = "localhost";
    const DB_USERNAME = "root";
    const DB_PASSWORD = "45330";
    const DB_PORT     = 3306;
    const DB_BASE     = "vi.ru";

    private $api;
    private $bot;
    private $token;
    public $db;

    function __construct (String $token, String $username) {
        $connect = new \mysqli(self::DB_HOST, self::DB_USERNAME, self::DB_PASSWORD, self::DB_BASE, self::DB_PORT);
        $this->token = $token;
        $this->api = new \PHPTelebot($token, $username);
        $this->bot = new Bot();
        $this->db  = new MysqliDb($connect);
        $this->db->autoReconnect = true;
    }

    public function callback( $callback ) {
        $chatID = $this->getMessage()['from']['id'];

        if( $callback == 'club:gold_rouse' ) {
            $this->deleteMessage($this->getMessage()['message']['message_id']);

            $this->sendMessage("Загрузка... Подождите пожалуйста...", ['chat_id'=>$chatID]);

            $file = new \CURLFile(realpath("photos/yppolus.jpg"));
            $this->sendPhoto($file, ['chat_id'=>$chatID]);
            $keyboard = [
                [
                    ['text' => 'Показать на карте', 'callback_data' => 'club:gold_rouse:map'],
                ],
                [
                    ['text' => 'Занятия', 'callback_data' => 'club:gold_rouse:work'],
                    ['text' => 'Тренеры', 'callback_data' => 'club:gold_rouse:trainers'],
                    ['text' => 'Массажисты', 'callback_data' => 'club:gold_rouse:masseurs'],
                ]
            ];
            $options = [
                'reply_markup' => ['inline_keyboard' => $keyboard],
                'chat_id'=>$chatID,
                'parsed_mode'=>"html"
            ];
            $message = "Золотая роза
Москва, ул. Новая, 151,ТРЦ «Сонная тетеря»
Пн-Пт:                 07:00 - 23:00         
Cб-Нд:                 09:00 - 22:00         
+7 (918) 13*-**-**";
            $this->sendMessage($message, $options);
        }
        if( $callback == "club:gold_rouse:map" ) {
            $this->sendLocation(-7.61, 109.51, $chatID);
        }
        if( $callback == "club:gold_rouse:work" ) {
            $this->toBookCalendar($callback);
        }
        if( $callback == "club:gold_rouse:trainers" ) {
            $data = $this->db->get('trainers');
            $countAll = count($data);
            $countMan = 0;
            $firstManID=0;
            $firstWomanID=0;
            $countWoman = 0;
            foreach ($data as $key => $value) {
                if ($value['sex'] == 0) {
                    $countMan += 1;
                    $firstManID = $key;
                } else {
                    $countWoman += 1;
                    $firstWomanID = $key;
                }
            }
            $keyboard = [
                [
                    ['text' => 'Все тренеры (' . $countAll . ')', 'callback_data' => 'club:gold_rouse:trainers:all:' . $data[0]['id'] . ':' . $countAll],
                ],
                [
                    ['text' => '❤️Женщины (' . $countWoman . ')', 'callback_data' => 'club:gold_rouse:trainers:woman:' . $data[$firstWomanID]['id'] . ':' . $countWoman],
                    ['text' => '🖤Мужчины (' . $countMan . ')', 'callback_data' => 'club:gold_rouse:trainers:man:' . $data[$firstManID]['id'] . ':'. $countMan],
                ]
            ];
            $options = [
                'reply_markup' => ['inline_keyboard' => $keyboard],
                'chat_id' => $chatID,
                'parsed_mode' => "html"
            ];
            $message = "🏅 Тренеры клуба Золотая роза";
            $this->sendMessage($message, $options);
        }
        print_r(explode(':', $callback));
        if( strpos($callback, ':') &&
            explode(':', $callback)[2] == "trainers" && explode(':', $callback)[3] == "all"
        ) {
            $data = $this->db->where('id', explode(':', $callback)[4])->where('club', explode(':', $callback)[1])->getOne('trainers');
            $file = new \CURLFile(realpath($data['photo']));
            $this->sendPhoto($file, ['chat_id'=>$chatID]);


            $next = $data['number']+1;
            echo "NEXT::::".$next;

            if( $next > 2 ) {
                $back = 'club:gold_rouse:trainers:all:' . ($data['number'] - 1) . ':' . explode(':', $callback)[5];
                $s1 = abs((explode(':', $callback)[4] -1));
                echo "BACK::::" . ($data['number'] - 1);
            } else {
                $back = "empty";
            }

            if ( (explode(':', $callback)[5]-explode(':', $callback)[4]) < 0 ) {
                $s = "";
            } else $s = (explode(':', $callback)[5]-explode(':', $callback)[4]) ;

            $keyboard = [
                [
                    ['text' => '<< '.$s1, 'callback_data' => $back],
                    ['text' => $data['id'], 'callback_data' => 'empty'],
                    ['text' => $s.' >>',
                        'callback_data' => 'club:gold_rouse:trainers:all:'.$next.':'.explode(':', $callback)[5]],
                ],
                [
                    ['text' => 'Записаться к тренеру', 'callback_data' => 'club:gold_rouse:trainers:woman']
                ]
            ];
            $options = [
                'reply_markup' => ['inline_keyboard' => $keyboard],
                'chat_id'=>$chatID,
                'parsed_mode'=>"html"
            ];
            $message = $data['name'].' '.$data['surname'] . " | " . $data['category'];
            $this->sendMessage($message, $options);
        }

        if( strpos($callback, ':') &&
            explode(':', $callback)[2] == "trainers" && explode(':', $callback)[3] == "woman" ) {
            $data = $this->db->where('id', explode(':', $callback)[4])->where('club', explode(':', $callback)[1])
                ->where('sex', 1)->getOne('trainers');
            $file = new \CURLFile(realpath($data['photo']));
            $this->sendPhoto($file, ['chat_id'=>$chatID]);


            $next = $data['number']+1;
            echo "NEXT::::".$next;

            if( $next > 2 ) {
                $back = 'club:gold_rouse:trainers:woman:' . ($data['number'] - 1) . ':' . explode(':', $callback)[5];
                $s1 = abs((explode(':', $callback)[4] -1));
                echo "BACK::::" . ($data['number'] - 1);
            } else {
                $s1="";
                $back = "empty";
            }

            if ( (explode(':', $callback)[5]-explode(':', $callback)[4]) < 0 ) {
                $s = "";
            } else $s = (explode(':', $callback)[5]-explode(':', $callback)[4]) ;

            $keyboard = [
                [
                    ['text' => '<< '.$s1, 'callback_data' => $back],
                    ['text' => $data['number'], 'callback_data' => 'empty'],
                    ['text' => $s.' >>',
                        'callback_data' => 'club:gold_rouse:trainers:woman:'.$next.':'.explode(':', $callback)[5]],
                ],
                [
                    ['text' => 'Записаться к тренеру', 'callback_data' => 'club:gold_rouse:trainers:woman']
                ]
            ];
            $options = [
                'reply_markup' => ['inline_keyboard' => $keyboard],
                'chat_id'=>$chatID,
                'parsed_mode'=>"html"
            ];
            $message = $data['name'].' '.$data['surname'] . " | " . $data['category'];
            $this->sendMessage($message, $options);
        }
        if( strpos($callback, ':') &&
            explode(':', $callback)[2] == "trainers" && explode(':', $callback)[3] == "man" ) {
            $data = $this->db->where('id', explode(':', $callback)[4])->where('club', explode(':', $callback)[1])
                ->where('sex', 0)->getOne('trainers');
            $file = new \CURLFile(realpath($data['photo']));
            $this->sendPhoto($file, ['chat_id'=>$chatID]);


            $next = $data['number']+1;
            echo "NEXT::::".$next;

            if( $next > 2 ) {
                $back = 'club:gold_rouse:trainers:man:' . ($data['number'] - 1) . ':' . explode(':', $callback)[5];
                $s1 = abs((explode(':', $callback)[4] -1));
                echo "BACK::::" . ($data['number'] - 1);
            } else {
                $back = "empty";
            }

            if ( (explode(':', $callback)[5]-explode(':', $callback)[4]) < 0 ) {
                $s = "";
            } else $s = (explode(':', $callback)[5]-explode(':', $callback)[4]) ;

            $keyboard = [
                [
                    ['text' => '<< '.$s1, 'callback_data' => $back],
                    ['text' => $data['number'], 'callback_data' => 'empty'],
                    ['text' => $s.' >>',
                        'callback_data' => 'club:gold_rouse:trainers:man:'.$next.':'.explode(':', $callback)[5]],
                ],
                [
                    ['text' => 'Записаться к тренеру', 'callback_data' => 'club:gold_rouse:trainers:women']
                ]
            ];
            $options = [
                'reply_markup' => ['inline_keyboard' => $keyboard],
                'chat_id'=>$chatID,
                'parsed_mode'=>"html"
            ];
            $message = $data['name'].' '.$data['surname'] . " | " . $data['category'];
            $this->sendMessage($message, $options);
        }
    }

    /**
     * Возвращает массив ответа.
     * @return array
     */
    public function getUpdates() {
        $getUpdates = [];
        return $this->api::$getUpdates;
    }

    /**
     * Возвращает массив ответа сообщения.
     * @return array
     */
    public function getMessage() {
        return $this->bot::message();
    }
    /**
     * Следить за определенным действием.
     * @param String $type
     * @param callable $answer
     * @return bool
     */
    public function on(String $type, callable $answer) {
        $this->api->on($type, $answer);
        return true;
    }
    /**
     * Принять команду
     * @param String $command
     * @param $answer
     * @return true
     */
    public function cmd(String $command, callable $answer) {
        $this->api->cmd($command, $answer);
        return true;
    }
    /**
     * Отправить сообщение
     * @param String $message
     * @param Int $chatID
     * @param array $options
     * @return mixed
     */
    public function sendMessage(String $message, Array $options = [], int $chatID = null) {
        if( $chatID != null )  $options = array_merge($options, ['chat_id'=>$chatID]);
            return $this->bot::sendMessage($message, $options);

    }

    /**
     * Отправляет фотографию URL или FILE
     * @param $photo
     * @param array $options
     * @param int|null $chatID
     * @return mixed
     */
    public function sendPhoto($photo, Array $options = [], int $chatID = null) {
        if( $chatID != null )  $options = array_merge($options, ['chat_id'=>$chatID]);

        return $this->bot::sendPhoto($photo, $options);
    }

    public function editMessageReplyInlineMarkup(Array $keyboard, array $options = []) {
        $data = ['reply_markup'=>["inline_keyboard"=>$keyboard]];
        $options = array_merge($options, $data);
        return $this->bot::send("editMessageReplyMarkup", $options);
    }

    /**
     * Отправляет местоположение.
     * @param float $x
     * @param float $y
     * @param int|null $chatID
     * @return mixed
     */
    public function sendLocation(float $x, float $y, int $chatID = null) {

        return $this->bot::send("sendLocation", ["latitude"=>$x, "longitude"=>$y, 'chat_id'=>$chatID]);
    }


    /**
     * Удалить сообщение
     * @param int $messageID
     * @return mixed
     */
    public function deleteMessage(int $messageID) {
        $user = $this->getMessage()['from']['id'];
        $ch = curl_init();
        $options = [
            CURLOPT_URL => 'https://api.telegram.org/bot'.$this->token.'/deleteMessage?chat_id='.$user .'&message_id='.$messageID,
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
        ];
        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo curl_error($ch)."\n";
        }
        curl_close($ch);

        print_r($user);


    }

    /*
     * Выводит календарь бронирования
     */
    public function toBookCalendar(String $callback) {
        $chatID = $this->getMessage()['from']['id'];
        $cl = $callback;
        $months = [ 1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля', 5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
            9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
        ];
        if (!isset($y) OR $y < 1970 OR $y > 2037) $y= date("Y");
        if (!isset($m) OR $m < 1 OR $m > 12) $m= date("m");
        $mouthText = ($months[$m]);
        $dateNow = date('d', time()) . ' ' . $mouthText . $y;
        $month_stamp=mktime(0,0,0,$m,1,$y);
        $day_count=date("t",$month_stamp);
        $weekday=date("w",$month_stamp);
        if ($weekday==0) $weekday=7;
        $start=-($weekday-2);
        $last=($day_count+$weekday-1) % 7;
        if ($last==0) $end=$day_count; else $end=$day_count+7-$last;
        $i=0;    $l=1;
        $keyboard = [
            [
                ['text' => $dateNow, 'callback_data' => 'empty'],
            ],
            [
                ['text' => 'Пн', 'callback_data' => 'empty'], ['text' => 'Вт', 'callback_data' => 'empty'], ['text' => 'Ср', 'callback_data' => 'empty'],
                ['text' => 'Ср', 'callback_data' => 'empty'], ['text' => 'Пт', 'callback_data' => 'empty'], ['text' => 'Сб', 'callback_data' => 'empty'],
                ['text' => 'НД', 'callback_data' => 'empty']
            ],
        ];
        for ($d = $start; $d <= $end; $d++) {
            if (($i++ % 7) == 0) {
                $l += 1;
            }
            if ($d < 1 OR $d > $day_count) {
                $keyboard[$l][] = ["text" => " ", 'callback_data' => 'empty'];
            } else {
                $keyboard[$l][] = ['text' => $d, 'callback_data' => 'select_data:' . $d . ':' . $m . ':' . $y];

            }
        }
        $keyboard[] = [
            ['text'=>"Выбрать месяц", 'callback_data'=>'calendar_select_mouth'],
        ];

        $options = [
            'reply_markup' => ['inline_keyboard' => $keyboard],
            'chat_id'=>$chatID,
            'message_id'=>$this->getMessage()['message']['message_id'],
            'parsed_mode'=>"html"
        ];
        $this->sendMessage("Календарь", $options);


        $this->on('callback', function($callback) use ($chatID, $cl) {
            $param  = ['select_data', 'calendar_select_mouth', 'calendar_select_mouth__', 'calendar_select_day', 'to_book', 'get'];
            foreach ($param as $key => $value ) {
                $search = ( strpos($value, ':') ) ? explode(':', $value)[0] : $value;
                $in  = ( strpos($callback, ':') ) ? explode(':', $callback)[0] : $callback;
                if ( $search == $in ) {
                    if (strpos($callback, ":")) {
                        $keyboard = [];

                        $data = explode(':', $callback);
                        if ($data[0] == "select_data") $type = "select"; else
                            if ($data[0] == "calendar_select_mouth") $type = "mouth"; else
                                if ($data[0] == "calendar_select_mouth__") $type = "mouth__"; else
                                    if ($data[0] == "calendar_select_day") $type = "day"; else
                                        if ($data[0] == "to_book") $type = "to_book"; else
                                            $type = "get";
                    } elseif ($callback != null) {
                        $keyboard = [];

                        if ($callback == "select_data") $type = "select"; else
                            if ($callback == "calendar_select_mouth") $type = "mouth"; else
                                if ($callback == "calendar_select_mouth__") $type = "mouth__"; else
                                    if ($callback == "calendar_select_day") $type = "day"; else
                                        if ($callback == "to_book") $type = "to_book"; else
                                            $type = "get";
                    }


//            echo "TELEGRAM TYPE CALLBACK REQUEST: " .$type ."|EXPLODE [1]" . (int) explode(':', $callback)[1] . "|";
                    $months = [1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля', 5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                        9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                    ];


                    if (!isset($y) OR $y < 1970 OR $y > 2037)
                        $y = ($type == "get" || $type == "mouth__") ? date("Y") :
                            ($type == "select") ? date("Y") : date("Y");

                    if (!isset($m) OR $m < 1 OR $m > 12) $m =
                        ($type == "get") ? date("m") :
                            ($type == "mouth__") ? (int)explode(':', $callback)[1] :
                                ($type == "select") ? (int)explode(':', $callback)[2] : date("m");

                    if ($type == "mouth__") {
                        $m = (int)explode(':', $callback)[1];
                    }
                    $mouthText = ($months[$m]);
                    if (strpos($callback, ':')) {
                        $dateNow =
                            (($type == "get") ? date('d', time()) . ' ' . $mouthText :
                                ($type == "select") ? (int)explode(':', $callback)[1] . ' ' . $mouthText :
                                    ($type == "mouth__") ? (int)explode(':', $callback)[1] . ' ' . $mouthText :
                                        date('d', time()) . ' ' . $mouthText) . " " . $y;
                    } else {
                        $dateNow = date('d', time()) . " " . $months[(int)date('m', time())] . " " . $y;
                    }

                    $month_stamp = mktime(0, 0, 0, $m, 1, $y);
                    $day_count = date("t", $month_stamp);
                    $weekday = date("w", $month_stamp);
                    if ($weekday == 0) $weekday = 7;
                    $start = -($weekday - 2);
                    $last = ($day_count + $weekday - 1) % 7;
                    if ($last == 0) $end = $day_count; else $end = $day_count + 7 - $last;
                    $i = 0;
                    $l = 1;
                    $keyboard = [];

                    if ($type == "select" || $type == "mouth__" || $type == "get") {
                        $keyboard = [
                            [
                                ['text' => $dateNow, 'callback_data' => 'empty'],
                            ],
                            [
                                ['text' => 'Пн', 'callback_data' => 'empty'], ['text' => 'Вт', 'callback_data' => 'empty'], ['text' => 'Ср', 'callback_data' => 'empty'],
                                ['text' => 'Ср', 'callback_data' => 'empty'], ['text' => 'Пт', 'callback_data' => 'empty'], ['text' => 'Сб', 'callback_data' => 'empty'],
                                ['text' => 'НД', 'callback_data' => 'empty']
                            ],
                        ];
                    } elseif ($type == "day") {
                        $keyboard = [
                            [
                                ['text' => 'Время (часы)', 'callback_data' => 'empty'],
                                ['text' => 'Игровая площадка', 'callback_data' => 'empty'],
                            ]
                        ];
                    } elseif ($type == "mouth") {
                        $keyboard = [
                            [
                                ['text' => 'Декабрь', 'callback_data' => 'calendar_select_mouth__:12'],
                                ['text' => 'Январь', 'callback_data' => 'calendar_select_mouth__:1'],
                                ['text' => 'Февраль', 'callback_data' => 'calendar_select_mouth__:2']
                            ],
                            [
                                ['text' => 'Март', 'callback_data' => 'calendar_select_mouth__:3'],
                                ['text' => 'Апрель', 'callback_data' => 'calendar_select_mouth__:4'],
                                ['text' => 'Май', 'callback_data' => 'calendar_select_mouth__:5'],
                            ],
                            [
                                ['text' => 'Июнь', 'callback_data' => 'calendar_select_mouth__:6'],
                                ['text' => 'Июль', 'callback_data' => 'calendar_select_mouth__:7'],
                                ['text' => 'Август', 'callback_data' => 'calendar_select_mouth__:8'],
                            ],
                            [
                                ['text' => 'Сентябрь', 'callback_data' => 'calendar_select_mouth__:9'],
                                ['text' => 'Октябрь', 'callback_data' => 'calendar_select_mouth__:10'],
                                ['text' => 'Ноябрь', 'callback_data' => 'calendar_select_mouth__:11'],
                            ],
                        ];
                    }
                    echo "MOUTH NOW:" . $m;
                    if ($type == "select" || $type == "get" || $type == "mouth__") {
                        for ($d = $start; $d <= $end; $d++) {
                            if (($i++ % 7) == 0) {
                                $l += 1;
                            }
                            if ($d < 1 OR $d > $day_count) {
                                $keyboard[$l][] = ["text" => " ", 'callback_data' => 'empty'];
                            } else {
                                if ($type == "select") {
                                    if ($d == explode(':', $callback)[1] && $m == explode(':', $callback)[2]) {
                                        $keyboard[$l][] = ['text' => '✅', 'callback_data' => 'select_data:' . $d . ':' . $m . ':' . $y];
                                        echo "SELECTED: " . $d . '-' . $m . '-' . $y;
                                    } else {
                                        $keyboard[$l][] = ['text' => $d, 'callback_data' => 'select_data:' . $d . ':' . $m . ':' . $y];
                                    }
                                } else {
                                    $keyboard[$l][] = ['text' => $d, 'callback_data' => 'select_data:' . $d . ':' . $m . ':' . $y];
                                }
                            }
                        }
                    } elseif ($type == "day") {
                        $date = explode(':', $callback);
                        $time = 7;
                        for ($i = 1; $i <= 18; $i++) {
                            $keyboard[$i][] = ['text' => $time . ':00', 'callback_data' => 'empty'];
                            $keyboard[$i][] = ['text' => 'FREE', 'callback_data' => 'to_book:' . $date[1] . ':' . $date[2] . ':' . $date[3] . ':' . $time];
                            $time++;
                        }
                    }

                    if ($type == "get" || $type == "mouth__") {
                        $keyboard[] = [
                            ['text' => "Выбрать месяц", 'callback_data' => 'calendar_select_mouth'],
                        ];
                    } elseif ($type == "select") {
                        $keyboard[] = [
                            ['text' => "Выбрать месяц", 'callback_data' => 'calendar_select_mouth'],
                            ['text' => "Продолжить", 'callback_data' => 'calendar_select_day:' . explode(':', $callback)[1] . ':' . $m . ':' . $y],
                        ];
                    } elseif ($type == "day") {
                        $keyboard[] = [
                            ['text' => 'Назад к выбору даты', 'callback_data' => $cl]
                        ];
                    }
                    $options = [
                        'reply_markup' => ['inline_keyboard' => $keyboard],
                        'chat_id' => $chatID,
                        'message_id' => $this->getMessage()['message']['message_id'],
                        'parsed_mode' => "html"
                    ];

                    if ($type == "get") {
                        $this->sendMessage("Календарь", $options);
                    } elseif ($type != "to_book") {
                        $this->editMessageReplyInlineMarkup($keyboard, $options);
                    }

                    if ($type == "to_book") {
                        $data = explode(':', $callback);
                        $this->sendMessage('Вы хотите забронировать поле на ' . $data[1] . '.' . $data[2] . '.' . $data[3] . '. Время: ' . $data[4] . ':00', ['chat_id' => $chatID]);
                    }
                }
            }
            $this->callback($callback);
        });

    }



    /**
     * Run Telegram Bots
     * @return true
     */
    public function run() {
        return $this->api->run();
    }
}